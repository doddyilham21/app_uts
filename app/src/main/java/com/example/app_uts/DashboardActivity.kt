package com.example.app_uts

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_dashboard.*

class DashboardActivity : AppCompatActivity(), View.OnClickListener{

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnStore -> {
                var intent = Intent(this,SperpatActivity::class.java)
                startActivity(intent)
            }
            R.id.btnMaps -> {
                var intent = Intent(this,MapsActivity::class.java)
                startActivity(intent)
            }
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        btnStore.setOnClickListener(this)
        btnMaps.setOnClickListener(this)
    }
}